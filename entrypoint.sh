#!/bin/bash

mkdir -p ${OPENFIRE_DATA_DIR}/conf
mkdir -p ${OPENFIRE_DATA_DIR}/plugins
mkdir -p ${OPENFIRE_DATA_DIR}/resources/security
mkdir -p ${OPENFIRE_DATA_DIR}/embedded-db

cp -an ${OPENFIRE_DIR}/conf_org/. ${OPENFIRE_DATA_DIR}/conf
cp -an ${OPENFIRE_DIR}/plugins_org/. ${OPENFIRE_DATA_DIR}/plugins
cp -an ${OPENFIRE_DIR}/resources/security_org/. ${OPENFIRE_DATA_DIR}/resources/security

chown -R ${OPENFIRE_USER}:${OPENFIRE_USER} ${OPENFIRE_DATA_DIR}

if [ -d "/entrypoint.d" ];
then
    for extra in /entrypoint.d/*; do
        case "$extra" in
            *.sh)     . "$extra" ;;
        esac
        echo
    done
    unset extra
fi

if [ -n "${OF_DB_USER}" ]       \
    && [ -n "${OF_DB_PASS}" ]   \
    && [ -n "${OF_DB_URL}" ]    \
    && [ -n "${OF_DB_DRIVER}" ] \
    && [ -n "${OF_PROP_FQDN}" ] \
    && [ -n "${OF_PROP_XMPP_DOMAIN}" ];
then
cat > ${OPENFIRE_DIR}/conf/openfire.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<jive>
  <adminConsole>
    <port>9090</port>
    <securePort>9091</securePort>
  </adminConsole>
  <locale>en</locale>
  <connectionProvider>
    <className>org.jivesoftware.database.DefaultConnectionProvider</className>
  </connectionProvider>
  <database>
    <defaultProvider>
      <driver>${OF_DB_DRIVER}</driver>
      <serverURL>${OF_DB_URL}</serverURL>
      <username>${OF_DB_USER}</username>
      <password>${OF_DB_PASS}</password>
      <testSQL>select 1</testSQL>
      <testBeforeUse>false</testBeforeUse>
      <testAfterUse>false</testAfterUse>
      <testTimeout>500</testTimeout>
      <timeBetweenEvictionRuns>30000</timeBetweenEvictionRuns>
      <minIdleTime>900000</minIdleTime>
      <maxWaitTime>500</maxWaitTime>
      <minConnections>5</minConnections>
      <maxConnections>25</maxConnections>
      <connectionTimeout>1.0</connectionTimeout>
    </defaultProvider>
  </database>
  <setup>true</setup>
  <fqdn>${OF_PROP_FQDN}</fqdn>
  <xmpp>
    <domain>${OF_PROP_XMPP_DOMAIN}</domain>
  </xmpp>
</jive>
EOF
    chown openfire:openfire ${OPENFIRE_DIR}/conf/openfire.xml
fi

exec "$@"
