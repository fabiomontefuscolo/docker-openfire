FROM maven:3.6.2-jdk-8 as packager

ARG OPENFIRE_SOURCE_TGZ="https://github.com/igniterealtime/Openfire/archive/v4.5.2.tar.gz"

RUN mkdir /usr/src/openfire \
    && curl -s -L "${OPENFIRE_SOURCE_TGZ}" \
    | tar -C /usr/src/openfire --strip-components 1 -zxf - \
    && cd /usr/src/openfire \
    && mvn -B verify

COPY ./log4j2.xml /usr/local/openfire/conf/log4j2.xml

FROM openjdk:8-jre
COPY --from=packager /usr/src/openfire/distribution/target/distribution-base /usr/local/openfire
MAINTAINER Fabio Montefuscolo <fabio.montefuscolo@gmail.com>

WORKDIR /usr/local/openfire

ENV OPENFIRE_USER=openfire \
    OPENFIRE_DIR=/usr/local/openfire \
    OPENFIRE_DATA_DIR=/var/lib/openfire \
    OPENFIRE_LOG_DIR=/var/log/openfire

COPY ./entrypoint.sh /entrypoint.sh

RUN apt-get update -qq \
    && apt-get install -yqq sudo \
    && useradd -u 1000 -m -d ${OPENFIRE_DATA_DIR} ${OPENFIRE_USER} \
    && chown -R openfire:openfire ${OPENFIRE_DIR} \
    && mv ${OPENFIRE_DIR}/conf ${OPENFIRE_DIR}/conf_org \
    && mv ${OPENFIRE_DIR}/plugins ${OPENFIRE_DIR}/plugins_org \
    && mv ${OPENFIRE_DIR}/resources/security ${OPENFIRE_DIR}/resources/security_org \
    && ln -sfT ${OPENFIRE_DATA_DIR}/conf ${OPENFIRE_DIR}/conf \
    && ln -sfT ${OPENFIRE_DATA_DIR}/plugins ${OPENFIRE_DIR}/plugins \
    && ln -sfT ${OPENFIRE_DATA_DIR}/resources/security ${OPENFIRE_DIR}/resources/security \
    && ln -sfT ${OPENFIRE_DATA_DIR}/embedded-db ${OPENFIRE_DIR}/embedded-db \
    && mkdir /entrypoint.d/ \
    && rm -rf /var/lib/apt/lists/*

RUN chown -R openfire:openfire ${OPENFIRE_DIR}

VOLUME ["/entrypoint.d/", "${OPENFIRE_DATA_DIR}"]

ENTRYPOINT ["/entrypoint.sh"]
CMD sudo -u ${OPENFIRE_USER} /usr/local/openjdk-8/bin/java          \
        -server                                                     \
        -Dlog4j.configurationFile=${OPENFIRE_DIR}/conf/log4j2.xml   \
        -DopenfireHome=${OPENFIRE_DIR}                              \
        -Dopenfire.lib.dir=${OPENFIRE_DIR}/lib                      \
        -classpath ${OPENFIRE_DIR}/lib/startup.jar                  \
        -jar ${OPENFIRE_DIR}/lib/startup.jar
